# Hospital Telesitter

## Problem: bed sitters for delerium / at risk (fall, depression, etc) patients => 1 to 1 ratio, expensive, ~3.4 falls per 1000 patient days.

## Solution: Remote monitoring + dispatch system. Dispatcher monitors patient via wifi cams, sends event messages to responders (floor nurses). Responders go to patient room.

### Dispatcher class  
* patient room  
* event  
* dispatch status  
* responders  

### Responder class  
* patient room  
* event  

RSpec  
http://www.tutorialspoint.com/rspec/

Cucumber  
https://github.com/cucumber/cucumber/wiki


### Steps
Start from empty directory

```
#!command prompt


gem rspec install  
gem cucumber install  

rspec --init  
cucumber --init  

mkdir lib  
```

1. Create empty dispatcher.rb and responder.rb class definitions in /lib
2. Write cucumber dispatch.feature first -> that defines interaction and interface
3. Run cucumber in root dir
4. Copy suggested steps to dispatch_steps.rb and complete
5. Create responder_spec.rb in /spec and complete
6. Create dispatch_spec.rb in /spec and complete
7. Run rspec in root dir
8. Implement Dispatcher and Responder classes to pass rspec and cucumber
