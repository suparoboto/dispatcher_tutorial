Feature: dispatch call
  In order to prevent adverse patient events
  As a constant remote observer
  I will dispatch the patient room on witnessed events to responders

Scenario: dispatch call to responders
  Given I have 2 responders
  And I have 1 patient in room 404
  When I witness a fall
  Then 2 responders are notified with patient room