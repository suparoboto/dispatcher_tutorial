require "dispatcher"
require "responder"

Before do
  @dispatcher = Dispatcher.new
end

Given(/^I have (\d+) responders$/) do |arg1|
  #pending # Write code here that turns the phrase above into concrete actions
  1.upto(arg1.to_i) do |k|
    @dispatcher.responders.push(Responder.new)
  end
end

Given(/^I have (\d+) patient in room (\d+)$/) do |arg1, arg2|
  #pending # Write code here that turns the phrase above into concrete actions
  @dispatcher.patient_room = arg2.to_i
end

When(/^I witness a fall$/) do
  #pending # Write code here that turns the phrase above into concrete actions
  @dispatcher.patient_fall(true)
end

Then(/^(\d+) responders are notified with patient room$/) do |arg1|
  #pending # Write code here that turns the phrase above into concrete actions
  responders = @dispatcher.responders
  responders.each do |responder|
    expect(responder.event).to eq("fall")
    expect(responder.patient_room).to eq(@dispatcher.patient_room)
  end
end