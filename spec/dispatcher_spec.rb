require "spec_helper"
require "dispatcher"

describe Dispatcher do
  describe "new" do
    it "should create a new dispatcher" do
      @dispatcher = Dispatcher.new
      expect(@dispatcher.responders).to be_empty
      expect(@dispatcher.patient_room).to eq(nil)
      expect(@dispatcher.patient_fall?).to eq(false)
    end
  end
  
  describe "patient fall" do
    it "should send fall message" do
      @dispatcher = Dispatcher.new
      @dispatcher.patient_room = 123
      @dispatcher.patient_fall(true)
      expect(@dispatcher.event).to eq("fall")
      expect(@dispatcher.status).to eq(:sent)
    end
  end
  
  describe "patient safe" do
    it "should have no event message" do
      @dispatcher = Dispatcher.new
      @dispatcher.patient_room = 123
      expect(@dispatcher.patient_fall?).to eq(false)
      expect(@dispatcher.event).to eq("")
      expect(@dispatcher.status).to eq(:clear)
    end
  end
    
end