require "spec_helper"
require "responder"

describe Responder do
  # before(:each) do
  #   @responder = Responder.new
  # end
  
  describe "new" do
    it "should create a new responder" do
      @responder = Responder.new
      expect(@responder.patient_room).to eq(nil)
      expect(@responder.event).to eq("")
    end
  end
  
  describe "patient fall" do
    it "should have fall as event message" do
      @responder = Responder.new
      @responder.patient_room = 123
      @responder.event = "fall"
      expect(@responder.event).to eq("fall")
    end
  end
  
  describe "patient safe" do
    it "should have empty event message" do
      @responder = Responder.new
      @responder.patient_room = 123
      expect(@responder.event).to eq("")
    end
  end
end