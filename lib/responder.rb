class Responder
  attr_accessor :patient_room, :event
  
  def initialize
    @patient_room = nil
    @event = ""
  end
end