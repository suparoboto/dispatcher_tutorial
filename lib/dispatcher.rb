class Dispatcher
  attr_accessor :responders, :patient_room, :event, :status
  
  def initialize
    @responders = []
    @patient_room = nil
    @patient_fall = false
    @event = ""
    @status = :clear
  end
  
  def patient_fall?
    @patient_fall
  end
  
  def patient_fall(fall)
    @patient_fall = fall
    
    if @patient_fall
      @event = "fall"
      notify    
    end
  end
  
  def notify
    responders.each do |responder|
      responder.patient_room = @patient_room
      responder.event = "fall"
    end
    @status = :sent
  end
end